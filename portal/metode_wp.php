<div class="container-fluid">
    <div class="block-header">
        <ol class="breadcrumb breadcrumb-col-pink">
            <li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
            <li class="active"><i class="material-icons">location_city</i> Data Metode Weighted Product</li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        TABEL NORMALISASI
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table-bordered table-striped table-hover" style="font-size: 12px;">
                                    <tr>
                                        <td colspan="3" align="center" style="padding: 0px;">Table Referensi</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">Kriterian</td>
                                        <td style="padding: 0px;">Ket</td>
                                        <!-- <td style="padding: 0px;">Bobot</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">JARAK DENGAN PASAR (COST)</td>
                                        <td style="padding: 0px;">C1</td>
                                        <!-- <td style="padding: 0px;">-0.278</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">KEPADATAN PENDUKUK (BENEFIT)</td>
                                        <td style="padding: 0px;">C2</td>
                                        <!-- <td style="padding: 0px;">0.167</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">JARAK DENGAN PABRIK (COST)</td>
                                        <td style="padding: 0px;">C3</td>
                                        <!-- <td style="padding: 0px;">-0.222</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">JARAK DENGAN GUDANG YG SUDAH ADA (BENEFIT) &nbsp; &nbsp;&nbsp;</td>
                                        <td style="padding: 0px;">C4</td>
                                        <!-- <td style="padding: 0px;">0.222</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">HARGA TANAH (COST)</td>
                                        <td style="padding: 0px;">C5 &nbsp; &nbsp;&nbsp;</td>
                                        <!-- <td style="padding: 0px;">-0.111</td> -->
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">

                                <!-- Tempat bobot rumus -->
                            </div>
                        </div>
                        <hr style="border: 1px solid;">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Rumah</th>
                                    <th>(C1)</th>
                                    <th>(C2)</th>
                                    <th>(C3)</th>
                                    <th>(C4)</th>
                                    <th>(C5)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $tampilkan = mysqli_query($connect, "SELECT * FROM tbl_perumahan inner join tbl_normalisasi on tbl_normalisasi.id_perumahan=tbl_perumahan.id_perumahan ORDER BY total DESC");
                                foreach ($tampilkan as $data) {
                                ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_perumahan']; ?></td>
                                        <td><?php echo round($data['c1'], 2); ?></td>
                                        <td><?php echo round($data['c2'], 2); ?></td>
                                        <td><?php echo round($data['c3'], 2); ?></td>
                                        <td><?php echo round($data['c4'], 2); ?></td>
                                        <td><?php echo round($data['c5'], 2); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        TABEL NILAI KESELURUHAN
                    </h2>
                </div>
                <div class="body">

                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Rumah</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT * FROM tbl_perumahan inner join tbl_normalisasi on tbl_normalisasi.id_perumahan=tbl_perumahan.id_perumahan ORDER BY total DESC");
                            foreach ($tampilkan as $data) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['nama_perumahan']; ?></td>
                                    <td><?php echo round($data['total'], 2); ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>