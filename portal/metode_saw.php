<div class="container-fluid">
    <div class="block-header">
        <ol class="breadcrumb breadcrumb-col-pink">
            <li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
            <li class="active"><i class="material-icons">location_city</i> Data Metode Simple Additive Weighting </li>
        </ol>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        TABEL NORMALISASI
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table-bordered table-striped table-hover" style="font-size: 12px;">
                                    <tr>
                                        <td colspan="3" align="center" style="padding: 0px;">Table Referensi</td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">Kriterian</td>
                                        <td style="padding: 0px;">Ket</td>
                                        <!-- <td style="padding: 0px;">Bobot</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">JARAK DENGAN PASAR (COST)</td>
                                        <td style="padding: 0px;">C1</td>
                                        <!-- <td style="padding: 0px;">-0.278</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">KEPADATAN PENDUKUK (BENEFIT)</td>
                                        <td style="padding: 0px;">C2</td>
                                        <!-- <td style="padding: 0px;">0.167</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">JARAK DENGAN PABRIK (COST)</td>
                                        <td style="padding: 0px;">C3</td>
                                        <!-- <td style="padding: 0px;">-0.222</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">JARAK DENGAN GUDANG YG SUDAH ADA (BENEFIT) &nbsp; &nbsp;&nbsp;</td>
                                        <td style="padding: 0px;">C4</td>
                                        <!-- <td style="padding: 0px;">0.222</td> -->
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px;">HARGA TANAH (COST)</td>
                                        <td style="padding: 0px;">C5 &nbsp; &nbsp;&nbsp;</td>
                                        <!-- <td style="padding: 0px;">-0.111</td> -->
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">

                                <!-- Tempat bobot rumus -->
                            </div>
                        </div>
                        <hr style="border: 1px solid;">

                        <?php

                        $crMax = mysqli_query($connect, "SELECT 
                                        min(jarak_dengan_pasar) as min1, 
                                        max(kepadatan_penduduk) as max1,
                                        min(jarak_dengan_pabrik) as min2,
                                        max(jarak_dengan_gudang) as max2,
                                        min(harga_tanah) as min3
                                        FROM tbl_perumahan");
                        $maxmin = mysqli_fetch_array($crMax);
                        ?>

                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Rumah</th>
                                    <th>(C1)</th>
                                    <th>(C2)</th>
                                    <th>(C3)</th>
                                    <th>(C4)</th>
                                    <th>(C5)</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                $no = 1;
                                $tampilkan = mysqli_query($connect, "SELECT * FROM tbl_perumahan inner join tbl_normalisasi on tbl_normalisasi.id_perumahan=tbl_perumahan.id_perumahan ORDER BY total DESC");
                                foreach ($tampilkan as $data) {
                                ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_perumahan']; ?></td>
                                        <td><?php echo round($maxmin['min1'] / $data['jarak_dengan_pasar'], 2); ?></td>
                                        <td><?php echo round($data['kepadatan_penduduk'] / $maxmin['max1'], 2); ?></td>
                                        <td><?php echo round($maxmin['min2'] / $data['jarak_dengan_pabrik'], 2); ?></td>
                                        <td><?php echo round($data['jarak_dengan_gudang'] / $maxmin['max2'], 2); ?></td>
                                        <td><?php echo round($maxmin['min3'] / $data['harga_tanah'], 2); ?></td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        TABEL NILAI PREFERENSI
                    </h2>
                </div>
                <div class="body">

                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Rumah</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // C1=  10%, C2= 30 %, C3= 20%, C4= 20%, C5= 20%, total 100%
                            $bobot = array(0.1, 0.3, 0.2, 0.2, 0.2);
                            $no = 1;
                            $tampilkan = mysqli_query($connect, "SELECT * FROM tbl_perumahan inner join tbl_normalisasi on tbl_normalisasi.id_perumahan=tbl_perumahan.id_perumahan ORDER BY total DESC");
                            foreach ($tampilkan as $data) {
                                $rangking = round(
                                    (($maxmin['min1'] / $data['jarak_dengan_pasar']) * $bobot[0]) +
                                        (($data['kepadatan_penduduk'] / $maxmin['max1']) * $bobot[1]) +
                                        (($maxmin['min2'] / $data['jarak_dengan_pabrik']) * $bobot[2]) +
                                        (($data['jarak_dengan_gudang'] / $maxmin['max2']) * $bobot[3]) +
                                        (($maxmin['min3'] / $data['harga_tanah']) * $bobot[4]),
                                    3
                                )
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $data['nama_perumahan']; ?></td>
                                    <td><?php echo $rangking ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>