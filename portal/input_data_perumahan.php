<div class="container-fluid">
    <div class="block-header">
        <ol class="breadcrumb breadcrumb-col-pink">
            <li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
            <li class="active"><i class="material-icons">location_city</i> Data Perumahan</li>
        </ol>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        INPUT DATA PERUMAHAN
                    </h2>
                </div>
                <div class="body">
                    <form action="action/simpan_data_perumahan.php" method="POST">
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <label for="" class="font-11">RUMAH</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="nama_perumahan" class="form-control" placeholder="Rumah" required />
                                    </div>
                                </div>
                                <label for="" class="font-11">JARAK DENGAN PASAR (COST)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="c1" class="form-control" placeholder="" required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="font-11">KEPADATAN PENDUKUK (BENEFIT)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="c2" class="form-control" placeholder="" required />
                                    </div>
                                </div>
                                <label for="" class="font-11">JARAK DENGAN PABRIK (COST)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="c3" class="form-control" placeholder="" required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="font-11">JARAK DENGAN GUDANG YG SUDAH ADA (BENEFIT)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="c4" class="form-control" placeholder="" required />
                                    </div>
                                </div>
                                <label for="" class="font-11">HARGA TANAH (COST)</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="c5" class="form-control" placeholder="" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">SIMPAN DATA</button>
                    </form>
                    <hr style="border: 1px solid; border-color: indianred;">
                    <center>
                        <h3 class="font-underline">DATA RUMAH</h3>
                    </center>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Rumah</th>
                                    <th>Jarak_dengan_pasar(C1)</th>
                                    <th>Kepadatan_penduduk(C2)</th>
                                    <th>Jarak_dengan_pabrik(C3)</th>
                                    <th>Jarak_dengan_gudang(C4)</th>
                                    <th>Harga_tanah(C5)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $tampilkan = mysqli_query($connect, "SELECT * FROM tbl_perumahan  ORDER BY id_perumahan DESC");
                                foreach ($tampilkan as $data) {
                                ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $data['nama_perumahan']; ?></td>
                                        <td><?php echo $data['jarak_dengan_pasar']; ?></td>
                                        <td><?php echo $data['kepadatan_penduduk']; ?></td>
                                        <td><?php echo $data['jarak_dengan_pabrik']; ?></td>
                                        <td><?php echo $data['jarak_dengan_gudang']; ?></td>
                                        <td><?php echo $data['harga_tanah']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>