function GajiPokok() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('gaji_pokok').value))));
	if (document.getElementById('gaji_pokok').value == "") {
    	document.getElementById('gaji_pokok').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('gaji_pokok').focus();
        document.getElementById('gaji_pokok').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function TjStruktural() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('tj_struktural').value))));
	if (document.getElementById('tj_struktural').value == "") {
    	document.getElementById('tj_struktural').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('tj_struktural').focus();
        document.getElementById('tj_struktural').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function TjTransportasi() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('tj_transportasi').value))));
	if (document.getElementById('tj_transportasi').value == "") {
    	document.getElementById('tj_transportasi').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('tj_transportasi').focus();
        document.getElementById('tj_transportasi').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function TjFungsional() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('tj_fungsional').value))));
	if (document.getElementById('tj_fungsional').value == "") {
    	document.getElementById('tj_fungsional').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('tj_fungsional').focus();
        document.getElementById('tj_fungsional').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorMengajarReguler() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_mengajar_reguler').value))));
	if (document.getElementById('honor_mengajar_reguler').value == "") {
    	document.getElementById('honor_mengajar_reguler').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_mengajar_reguler').focus();
        document.getElementById('honor_mengajar_reguler').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorMengajarMandiri() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_mengajar_mandiri').value))));
	if (document.getElementById('honor_mengajar_mandiri').value == "") {
    	document.getElementById('honor_mengajar_mandiri').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_mengajar_mandiri').focus();
        document.getElementById('honor_mengajar_mandiri').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorMenggantikan() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_menggantikan').value))));
	if (document.getElementById('honor_menggantikan').value == "") {
    	document.getElementById('honor_menggantikan').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_menggantikan').focus();
        document.getElementById('honor_menggantikan').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorPA() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_pa').value))));
	if (document.getElementById('honor_pa').value == "") {
    	document.getElementById('honor_pa').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_pa').focus();
        document.getElementById('honor_pa').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorKompre() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_kompre').value))));
	if (document.getElementById('honor_kompre').value == "") {
    	document.getElementById('honor_kompre').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_kompre').focus();
        document.getElementById('honor_kompre').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorPemMagang() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_pem_magang').value))));
	if (document.getElementById('honor_pem_magang').value == "") {
    	document.getElementById('honor_pem_magang').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_pem_magang').focus();
        document.getElementById('honor_pem_magang').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorMengawasUjianMID() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_mengawas_ujian_mid').value))));
	if (document.getElementById('honor_mengawas_ujian_mid').value == "") {
    	document.getElementById('honor_mengawas_ujian_mid').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_mengawas_ujian_mid').focus();
        document.getElementById('honor_mengawas_ujian_mid').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorMengawasUAS() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_mengawas_uas').value))));
	if (document.getElementById('honor_mengawas_uas').value == "") {
    	document.getElementById('honor_mengawas_uas').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_mengawas_uas').focus();
        document.getElementById('honor_mengawas_uas').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorKoreksiUjianMID() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_koreksi_ujian_mid').value))));
	if (document.getElementById('honor_koreksi_ujian_mid').value == "") {
    	document.getElementById('honor_koreksi_ujian_mid').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_koreksi_ujian_mid').focus();
        document.getElementById('honor_koreksi_ujian_mid').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorKoreksiUAS() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_koreksi_uas').value))));
	if (document.getElementById('honor_koreksi_uas').value == "") {
    	document.getElementById('honor_koreksi_uas').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_koreksi_uas').focus();
        document.getElementById('honor_koreksi_uas').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function THR() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('thr').value))));
	if (document.getElementById('thr').value == "") {
    	document.getElementById('thr').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('thr').focus();
        document.getElementById('thr').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function Bonus() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('bonus').value))));
	if (document.getElementById('bonus').value == "") {
    	document.getElementById('bonus').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('bonus').focus();
        document.getElementById('bonus').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorLain() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_lain').value))));
	if (document.getElementById('honor_lain').value == "") {
    	document.getElementById('honor_lain').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_lain').focus();
        document.getElementById('honor_lain').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function BPJS() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('bpjs').value))));
	if (document.getElementById('bpjs').value == "") {
    	document.getElementById('bpjs').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('bpjs').focus();
        document.getElementById('bpjs').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function PPh() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('pph').value))));
	if (document.getElementById('pph').value == "") {
    	document.getElementById('pph').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('pph').focus();
        document.getElementById('pph').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function Kehadiran() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('kehadiran').value))));
	if (document.getElementById('kehadiran').value == "") {
    	document.getElementById('kehadiran').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('kehadiran').focus();
        document.getElementById('kehadiran').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function PotLain() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('pot_lain').value))));
	if (document.getElementById('pot_lain').value == "") {
    	document.getElementById('pot_lain').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('pot_lain').focus();
        document.getElementById('pot_lain').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function HonorPiket() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('honor_piket').value))));
	if (document.getElementById('honor_piket').value == "") {
    	document.getElementById('honor_piket').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('honor_piket').focus();
        document.getElementById('honor_piket').value = tandaPemisahTitik(angka) ;
        return false;
    }
}

function Lembur() {
	var angka = bersihPemisah(bersihPemisah(bersihPemisah(bersihPemisah(document.getElementById('lembur').value))));
	if (document.getElementById('lembur').value == "") {
    	document.getElementById('lembur').focus();
    		return false;
    }else if (angka >= 1) {
    	document.getElementById('lembur').focus();
        document.getElementById('lembur').value = tandaPemisahTitik(angka) ;
        return false;
    }
}